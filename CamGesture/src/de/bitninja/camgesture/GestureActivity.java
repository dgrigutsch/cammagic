package de.bitninja.camgesture;

import java.io.IOException;

import android.hardware.Camera;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.Menu;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class GestureActivity extends Activity {
	
	Camera myCamera;
	SurfaceView mySurfaceView;
	SurfaceHolder mySurfaceHolder;
	Button start;
	boolean isPreview;
	boolean toggleStart;
	SensorManager mySensorManager;
	Sensor myLightSensor;
	Long timer;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_gesture);
		isPreview = false;
        mySurfaceView = (SurfaceView)findViewById(R.id.mypreview);
        mySurfaceHolder = mySurfaceView.getHolder();
        mySurfaceHolder.addCallback(mySurfaceCallback);
        mySurfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        start = (Button)findViewById(R.id.btn_camStart);
        start.setOnClickListener(OnStartClick);
        toggleStart = false;
       
        mySensorManager = (SensorManager)getSystemService(Context.SENSOR_SERVICE);
        myLightSensor = mySensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);       
        mySensorManager.registerListener(lightSensorEventListener,myLightSensor,SensorManager.SENSOR_DELAY_NORMAL);
 
	}
	
	
	SensorEventListener lightSensorEventListener = new SensorEventListener(){

		   @Override
		   public void onAccuracyChanged(Sensor sensor, int arg1) { 
			   
		   }
		
		   @Override
		   public void onSensorChanged(SensorEvent event) {
			   if(event.sensor.getType()==Sensor.TYPE_LIGHT){
				   Log.e("Sensor", Float.valueOf(event.values[0]).toString());
				   if(event.values[0] <= 12f){
					   startCam();
				   }
			   }
   }};
	
	SurfaceHolder.Callback mySurfaceCallback = new SurfaceHolder.Callback(){

		  @Override
		  public void surfaceChanged(SurfaceHolder holder, int format, int width,int height) {
			  try {
				  myCamera.setPreviewDisplay(mySurfaceHolder);
				  Camera.Parameters myParameters = myCamera.getParameters();
				  myCamera.setDisplayOrientation(90);
			  } catch (IOException e) {
				  e.printStackTrace();
			  } 
				  
		  }
		
		  @Override
		  public void surfaceCreated(SurfaceHolder holder) {
			  try {
				  Log.e("mm", "mm");
				  myCamera.setPreviewDisplay(mySurfaceHolder);
			  } catch (IOException e) {
				  e.printStackTrace();
			  } 
		  }
		  @Override
		  public void surfaceDestroyed(SurfaceHolder holder) {
		  }
	};
	
	
	View.OnClickListener OnStartClick = new OnClickListener() {		
		@Override
		public void onClick(View v) {
			if(toggleStart == false){
				startCam();
				
			} else {
				stopCam();
			}
		}
	};
	
	public void startCam(){
		timer = System.currentTimeMillis();
		isPreview = true;
		toggleStart = true;
		start.setText("Stop");
		myCamera.startPreview();
	}
	public void stopCam(){
		toggleStart = false;
		start.setText("Start");
		//mySurfaceHolder.removeCallback(mySurfaceCallback);
		myCamera.stopPreview();
		isPreview = false; 
	}
 
 @Override
 protected void onResume() {
	 super.onResume();
	 myCamera = Camera.open(1);
 }

 @Override
 protected void onPause() {
	 if(isPreview){
		 myCamera.stopPreview();
	 }
  
	 myCamera.release();
	 myCamera = null;
	 isPreview = false; 
	 super.onPause();
 } 
}
